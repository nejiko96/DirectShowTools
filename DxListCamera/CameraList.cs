﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using DirectShowLib;

namespace DxListCamera
{
    class CameraList
    {
        public static int SearchCamera(string name)
        {
            DsDevice[] capDevices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
            for (int i = 0; i < capDevices.Count(); i++)
            {
                DsDevice capDevice = capDevices[i];

                string desc = capDevice.GetPropBagValue("Description");
                if (desc != null && desc.IndexOf(name) >= 0)
                {
                    return i;
                }

                string fname = capDevice.GetPropBagValue("FriendlyName");
                if (fname != null && fname.IndexOf(name) >= 0)
                {
                    return i;
                }
            }
            return -1;
        }

        public static void ListCamera(int no, bool oneFlg)
        {
            DsDevice[] capDevices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);

            if (no >= 0)
            {
                showCamera(capDevices[no], no, oneFlg, true);
            }
            else
            {
                for (int i = 0; i < capDevices.Count(); i++)
                {
                    showCamera(capDevices[i], i, oneFlg, false);
                }
            }
        }

        private static void showCamera(DsDevice capDevice, int no, bool oneFlg, bool detailFlg)
        {
            string desc = capDevice.GetPropBagValue("Description");
            string fname = capDevice.GetPropBagValue("FriendlyName");

            if (!detailFlg)
            {
                Console.WriteLine("No.{0} :\"{1}\"", no, desc ?? fname ?? "");
                return;
            }

            Console.WriteLine("Camera No.{0} :", no);
            if (desc != null) Console.WriteLine("Description : \"{0}\"", desc);
            if (fname != null) Console.WriteLine("FriendlyName : \"{0}\"", fname);

            int hr, hr2;

            object source;
            Guid iid = typeof(IBaseFilter).GUID;
            IBaseFilter src;
            capDevice.Mon.BindToObject(null, null, ref iid, out source);
            src = (IBaseFilter) source;

            FilterInfo filterInfo;
            hr = src.QueryFilterInfo(out filterInfo);
            if (hr == 0) Console.WriteLine("Filter name : \"{0}\"", filterInfo.achName);

            string vendor;
            hr = src.QueryVendorInfo(out vendor);
            if (hr == 0) Console.WriteLine("Vendor name : \"{0}\"", vendor);

            if (src is IAMCameraControl)
            {
                Console.WriteLine("IAMCameraControl Enable.");
                Console.WriteLine("Min\tCurrent\tMax\tStep\tDefault\tFlags\tName");
                IAMCameraControl camCtrl = (IAMCameraControl)src;
                foreach (CameraControlProperty ccp in Enum.GetValues(typeof(CameraControlProperty)))
                {
                    int iValue, iMin, iMax, iDelta, iDefault;
                    CameraControlFlags nowFlag, capsFlag;
                    hr = camCtrl.Get(ccp, out iValue, out nowFlag);
                    hr2 = camCtrl.GetRange(ccp, out iMin, out iMax, out iDelta, out iDefault, out capsFlag);
                    if (hr == 0 && hr2 == 0)
                    {
                        Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}/{6}\t{7}",
                            iMin, iValue, iMax, iDelta, iDefault, nowFlag.ToString(), capsFlag.ToString(), ccp.ToString());
                    }
                }
            }
            else
            {
                Console.WriteLine("QueryInterface IAMCameraControl failed.");
            }

            if (src is IAMVideoProcAmp)
            {
                Console.WriteLine("IAMVideoProcAmp Enable.");
                Console.WriteLine("Min\tCurrent\tMax\tStep\tDefault\tFlags\tName");
                IAMVideoProcAmp vidProc = (IAMVideoProcAmp) src;
                foreach (VideoProcAmpProperty vpa in Enum.GetValues(typeof(VideoProcAmpProperty)))
                {
                    int iValue, iMin, iMax, iDelta, iDefault;
                    VideoProcAmpFlags nowFlag, capsFlag;
                    hr = vidProc.Get(vpa, out iValue, out nowFlag);
                    hr2 = vidProc.GetRange(vpa, out iMin, out iMax, out iDelta, out iDefault, out capsFlag);
                    if (hr == 0 && hr2 == 0)
                    {
                        Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}/{6}\t{7}",
                            iMin, iValue, iMax, iDelta, iDefault, nowFlag.ToString(), capsFlag.ToString(), vpa.ToString());
                    }
                }
            }
            else
            {
                Console.WriteLine("QueryInterface IAMVideoProcAmp failed.");
            }

            if (src is IAMVideoControl)
            {
                Console.WriteLine("IAMVideoControl Enable.");
                IAMVideoControl vidCtrl = (IAMVideoControl) src;
                IPin pSrcOutPin = DsFindPin.ByDirection(src, PinDirection.Output, 0);
                if (pSrcOutPin != null)
                {
                    VideoControlFlags pCapsFlags;
                    hr = vidCtrl.GetCaps(pSrcOutPin, out pCapsFlags);
                    if (hr == 0)
                    {
                        Console.WriteLine("VideoControlFlags capabilities : {0:X4}", (int) pCapsFlags);
                        if ((pCapsFlags & VideoControlFlags.FlipHorizontal) > 0) { Console.WriteLine("\tVideoControlFlag_FlipHorizontal"); }
                        if ((pCapsFlags & VideoControlFlags.FlipVertical) > 0) { Console.WriteLine("\tVideoControlFlag_FlipVertical"); }
                        if ((pCapsFlags & VideoControlFlags.ExternalTriggerEnable) > 0) { Console.WriteLine("\tVideoControlFlag_ExternalTriggerEnable"); }
                        if ((pCapsFlags & VideoControlFlags.Trigger) > 0) { Console.WriteLine("\tVideoControlFlag_Trigger"); }
                    }
                }
            }
            else
            {
                Console.WriteLine("QueryInterface IAMVideoControl failed.");
            }

            if (src is IAMDroppedFrames)
            {
                Console.WriteLine("IAMDroppedFrames Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IAMDroppedFrames failed.");
            }

            if (src is IAMExtDevice)
            {
                Console.WriteLine("IAMExtDevice Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IAMExtDevice failed.");
            }

            if (src is ISpecifyPropertyPages)
            {
                Console.WriteLine("ISpecifyPropertyPages Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface ISpecifyPropertyPages failed.");
            }

            if (src is IPropertyBag)
            {
                Console.WriteLine("IPropertyBag Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IPropertyBag failed.");
            }

            if (src is IAMAnalogVideoDecoder)
            {
                Console.WriteLine("IAMAnalogVideoDecoder Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IAMAnalogVideoDecoder failed.");
            }

            if (src is IAMBufferNegotiation)
            {
                Console.WriteLine("IAMBufferNegotiation Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IAMBufferNegotiation failed.");
            }

            if (src is IAMCopyCaptureFileProgress)
            {
                Console.WriteLine("IAMCopyCaptureFileProgress Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IAMCopyCaptureFileProgress failed.");
            }

            if (src is IAMCrossbar)
            {
                Console.WriteLine("IAMCrossbar Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IAMCrossbar failed.");
            }

            //if (src is IAMVfwCaptureDialogs)
            //{
            //    Console.WriteLine("IAMVfwCaptureDialogs Enable.");
            //}
            //else
            //{
            //    Console.WriteLine("QueryInterface IAMVfwCaptureDialogs failed.");
            //}

            if (src is IAMStreamControl)
            {
                Console.WriteLine("IAMStreamControl Enable.");
            }
            else
            {
                Console.WriteLine("QueryInterface IAMStreamControl failed.");
            }

            if (oneFlg)
            {
                // 出力に接続可能なメディアタイプを列挙する
                Console.WriteLine("Listup MediaType");
                IPin pSrcOutPin = DsFindPin.ByDirection(src, PinDirection.Output, 0);
                listPinMediaTypes(pSrcOutPin);
            }
            else
            {
                // 出力に接続可能なメディアタイプを列挙する
                Console.WriteLine("---- Listup output Pin MediaType");
                listPins(src, PinDirection.Output, listPinMediaTypes);

                // 入力に接続可能なメディアタイプを列挙する
                Console.WriteLine("---- Listup input Pin MediaType");
                listPins(src, PinDirection.Input, listPinMediaTypes);

            }
        }

        private static void listPins(IBaseFilter src, PinDirection pDir, Action<IPin> func)
        {
            int hr;
            IEnumPins enumPins = null;
            IPin[] pins = new IPin[1];
            PinInfo info;

            try
            {
                hr = src.EnumPins(out enumPins);
                if (hr != 0) return;

                while (enumPins.Next(pins.Length, pins, IntPtr.Zero) == 0)
                {
                    hr = pins[0].QueryPinInfo(out info);
                    if (hr != 0) { continue; }

                    if (info.dir != pDir) { continue; }

                    Console.WriteLine("Pin name : \"{0}\"", info.name);
                    if (func != null)
                    {
                        func(pins[0]);
                    }
                }
            }
            finally
            {
                if (enumPins != null)
                {
                    Marshal.ReleaseComObject(enumPins);

                }
            }
        }

        private static void listPinMediaTypes(IPin pin)
        {
            int hr;
            IEnumMediaTypes enumMediaTypes = null;
            AMMediaType[] mediaTypes = new AMMediaType[1];

            try
            {
                hr = pin.EnumMediaTypes(out enumMediaTypes);
                if (hr != 0) return;

                while (enumMediaTypes.Next(mediaTypes.Length, mediaTypes, IntPtr.Zero) == 0)
                {
                    MediaTypeDisp.DispMediaType(mediaTypes[0]);
                }
            }
            finally
            {
                if (enumMediaTypes != null)
                {
                    Marshal.ReleaseComObject(enumMediaTypes);

                }
            }

        }
    }
}
