﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using DirectShowLib;

namespace DxListCamera
{
    public static class DictionaryExtensions
    {
        public static TValue GetValueOrDefault<TKey, TValue>(
          this IDictionary<TKey, TValue> source, TKey key
        )
        {
            if (source == null || source.Count == 0)
            {
                return default(TValue);
            }

            TValue result;
            if (source.TryGetValue(key, out result))
            {
                return result;
            }
            return default(TValue);
        }
    }

    public static class GuidExtensions
    {
        public static bool isFourCC(this Guid src)
        {
            return src.ToString().ToUpper().EndsWith("-0000-0010-8000-00AA00389B71");
        }

        public static string GetFourCCCode(this Guid src)
        {
            byte[] bytes = src.ToByteArray();
            char[] chars = new char[] { (char) bytes[0], (char) bytes[1], (char) bytes[2], (char) bytes[3] };
            return new string(chars);
        }
    }

    class MediaTypeDisp
    {
        private const string UNKNOWN_GUID = "Unknown.{0}";

        private const string ONLY_GUID = "{0}";

        private const string FOURCC_FMT = "(FOURCC'{0}')";

        private static readonly Dictionary<Guid, string> MAJOR_TYPE_TBL
            = new Dictionary<Guid, string>
        {
            { MediaType.Audio,  "Audio" },
            { MediaType.Video,  "Video" },
            { MediaType.Stream, "Stream" },
            { Guid.Empty,       "GUID NULL" },
        };

        private static readonly Dictionary<Guid, string> SUB_TYPE_TBL
            = new Dictionary<Guid, string>
        {
            { MediaSubType.PCM,             "PCMオーディオ" },
            { MediaSubType.MPEG1Packet,     "MPEG1Packet" },
            { MediaSubType.MPEG1Payload,    "MPEG1Payload" },
            { MediaSubType.Mpeg2Audio,      "MPEG-2オーディオ" },
            //{ MediaSubType.DVD_LPCM_AUDIO, "DVDオーディオ" },
            { MediaSubType.DRM_Audio,       "WAVE_FORMAT_DRMに対応" },
            { MediaSubType.IEEE_FLOAT,      "WAVE_FORMAT_IEEE_FLOATに対応" },
            { MediaSubType.DolbyAC3,        "Dolby データ。" },
            { MediaSubType.DOLBY_AC3_SPDIF, "SPDIFのDolbyAC3" },
            { MediaSubType.RAW_SPORT,       "MEDIASUBTYPE_DOLBY_AC3_SPDIFと同等" },
            { MediaSubType.SPDIF_TAG_241h,  "MEDIASUBTYPE_DOLBY_AC3_SPDIFと同等" },
            { MediaSubType.RGB1,             "RGB1(1bpp)" },
            { MediaSubType.RGB4,            "RGB4(4bpp)" },
            { MediaSubType.RGB8,            "RGB8(8bpp)" },
            { MediaSubType.RGB555,          "RGB555(16bpp)" },
            { MediaSubType.RGB565,          "RGB565(16bpp)" },
            { MediaSubType.RGB24,           "RGB24(24bpp)" },
            { MediaSubType.RGB32,           "RGB32(32bpp)" },
            { MediaSubType.ARGB32,          "ARGB(32 bpp)" },
            { MediaSubType.AYUV,            "AYUV(4:4:4)" },
            { MediaSubType.UYVY,            "UYVY(4:2:2)" },
            { MediaSubType.Y411,            "Y411(4:1:1)" },
            { MediaSubType.Y41P,            "Y41P(4:1:1)" },
            { MediaSubType.Y211,            "Y211" },
            { MediaSubType.YUY2,            "YUY2(4:2:2)" },
            { MediaSubType.YVYU,            "YVYU(4:2:2)" },
            { MediaSubType.YUYV,            "YUYV" },
            { MediaSubType.CFCC,            "MJPG('CFCC')" },
            { MediaSubType.CLJR,            "CLJR('CLJR')" },
            { MediaSubType.CPLA,            "CinepakUYVY('CPLA')" },
            { MediaSubType.CLPL,            "CirrusLogicYUV('CLPL')" },
            { MediaSubType.IJPG,            "IntergraphJPEG('IJPG')" },
            { MediaSubType.MDVF,            "DVencoded('MDVF')" },
            { MediaSubType.MJPG,            "MotionJPEG('MJPG')" },
            { MediaSubType.Overlay,         "ハードウェアオーバーレイを使って配信されるビデオ" },
            { MediaSubType.PLUM,            "PlumMJPG('Plum')" },
            { MediaSubType.QTJpeg,          "Quicktime JPEG 圧縮" },
            { MediaSubType.QTMovie,         "Apple(c)QuickTime(c)圧縮" },
            { MediaSubType.QTRle,           "QuicktimeRLE圧縮" },
            { MediaSubType.QTRpza,          "QuicktimeRPZA 圧縮" },
            { MediaSubType.QTSmc,           "QuicktimeSMC 圧縮" },
            { MediaSubType.TVMJ,            "TrueVisionMJPG('TVMJ')" },
            { MediaSubType.VPVBI,           "ビデオポート垂直同期間隔(VBI)" },
            { MediaSubType.VPVideo,         "ビデオポートビデオ" },
            { MediaSubType.WAKE,            "一部のカードによって生成されるMJPG('WAKE')" },
            // MPEG2 stream
            { MediaSubType.Mpeg2Video,      "MPEG2_VIDEO" },
            { MediaSubType.Mpeg2Program,    "MPEG2_PROGRAM" },
            { MediaSubType.Mpeg2Transport,  "MPEG2_TRANSPORT" },
            // -----------------------------以下、不明
            { MediaSubType.Avi,             "Avi" },
            { Guid.Empty,                   "GUID NULL" },
        };

        private static readonly Dictionary<Guid, string> FORMAT_TYPE_TBL
            = new Dictionary<Guid, string>
        {
            { FormatType.None,		    "None"},
            { FormatType.DvInfo,		"Dv"},		// DvInfo
            { FormatType.MpegVideo,		"MPEG"},	// MPEGVideo
            { FormatType.Mpeg2Video,	"MPEG2"},	// MPEG2Video
            { FormatType.VideoInfo,		"Video"},	// VideoInfo
            { FormatType.VideoInfo2,	"Video2"},	// VideoInfo2
            { FormatType.WaveEx,	    "Wave"},	// WaveFormatEx
            { Guid.Empty,               "GUID NULL" },
        };

        private static readonly AMInterlace VIH2_FLAGS_MASK = AMInterlace.IsInterlaced | AMInterlace.OneFieldPerSample | AMInterlace.Field1First;

        private static readonly Dictionary<Enum, string> VIH2_FLAGS
            = new Dictionary<Enum, string>
        {
            { AMInterlace.IsInterlaced,         "Interlaced" },
            { AMInterlace.OneFieldPerSample,    "1Field" },
            { AMInterlace.Field1First,          "Field1First" },
        };

        private static readonly Dictionary<Enum, string> VIH2_FIELD_PATTERNS
            = new Dictionary<Enum, string>
        {
            { AMInterlace.FieldPatField1Only,         "Field1Only" },
            { AMInterlace.FieldPatField2Only,         "Field2Only" },
            { AMInterlace.FieldPatBothRegular,        "BothRegular" },
            { AMInterlace.FieldPatBothIrregular,      "BothIrregular" },
        };

        private static readonly Dictionary<Enum, string> VIH2_DISPLAY_MODES
            = new Dictionary<Enum, string>
        {
            { AMInterlace.DisplayModeBobOnly,         "BobOnly" },
            { AMInterlace.DisplayModeWeaveOnly,       "WeaveOnly" },
            { AMInterlace.DisplayModeBobOrWeave,      "BobOrWeave" },
        };
                
        private static string GetMediaTypeStr(Guid guid, Dictionary<Guid, string> dic, string defaultFmt)
        {
            string ret = dic.GetValueOrDefault(guid) ?? string.Format(defaultFmt, guid.ToString("B"));

            if (guid.isFourCC())
            {
                ret += String.Format(FOURCC_FMT, guid.GetFourCCCode());
            }

            return ret;
        }

        private static string GetFlagStr(Enum flag, Dictionary<Enum, string> dic)
        {
            string s = dic.GetValueOrDefault(flag);
            if (string.IsNullOrEmpty(s))
            {
                return "";
            }
            else
            {
                return String.Format("({0})", s);
            }
        }

        private static void GetBitmapInfoStr(BitmapInfoHeader bmi, StringBuilder sb)
        {
            sb.Append(String.Format("({0},{1})", bmi.Width, bmi.Height));
        }

        private static void GetVideoInfoStr(VideoInfoHeader vih, StringBuilder sb)
        {
            sb.Append(String.Format("[{0:F2}fps({1:F4}ms)]", 10000000.0 / (vih.AvgTimePerFrame), vih.AvgTimePerFrame / 10000.0));
            if (vih.BitRate != 0)
            {
                sb.Append(String.Format("({0:F2}Mbps)", ((double) vih.BitRate)/1000000.0));
            }
        }

        private static void GetVideoInfo2Str(VideoInfoHeader2 vih2, StringBuilder sb)
        {
            sb.Append(String.Format("[{0:F2}fps({1:F4}ms)]", 10000000.0 / (vih2.AvgTimePerFrame), vih2.AvgTimePerFrame / 10000.0));
            if (vih2.BitRate != 0)
            {
                sb.Append(String.Format("({0:F2}Mbps)", ((double)vih2.BitRate) / 1000000.0));
            }
            sb.Append(GetFlagStr(vih2.InterlaceFlags & VIH2_FLAGS_MASK, VIH2_FLAGS));
            sb.Append(GetFlagStr(vih2.InterlaceFlags & AMInterlace.FieldPatternMask, VIH2_FIELD_PATTERNS));
            sb.Append(GetFlagStr(vih2.InterlaceFlags & AMInterlace.DisplayModeMask, VIH2_DISPLAY_MODES));
        }

        private static void GetWaveFormatStr(WaveFormatEx wf, StringBuilder sb)
        {
            if (wf.wFormatTag == 0x0001)
            {
                sb.Append(" WAVE_FORMAT_PCM");
            }
            sb.Append(String.Format("({0}ch)", wf.nChannels));
            sb.Append(String.Format("({0}SamplesPerSec)", wf.nSamplesPerSec));
            sb.Append(String.Format("({0}bits)", wf.wBitsPerSample));
            sb.Append(String.Format("({0})", wf.nBlockAlign));
            sb.Append(String.Format("({0})", wf.cbSize));
        }

        public static void DispMediaType(AMMediaType mt)
        {
            string majorTypeStr = GetMediaTypeStr(mt.majorType, MAJOR_TYPE_TBL, UNKNOWN_GUID);
            string subTypeStr = GetMediaTypeStr(mt.subType, SUB_TYPE_TBL, UNKNOWN_GUID);
            string formatTypeStr = GetMediaTypeStr(mt.formatType, FORMAT_TYPE_TBL, ONLY_GUID);

            BitmapInfoHeader bmi = null;
            VideoInfoHeader vih = null;
            VideoInfoHeader2 vih2 = null;
            WaveFormatEx wf = null;

            if (mt.formatType == FormatType.VideoInfo || mt.formatType == FormatType.MpegVideo)
            {
                vih = new VideoInfoHeader();
                Marshal.PtrToStructure(mt.formatPtr, vih);
                bmi = vih.BmiHeader;

            }

            if (mt.formatType == FormatType.VideoInfo2 || mt.formatType == FormatType.Mpeg2Video)
            {
                vih2 = new VideoInfoHeader2();
                Marshal.PtrToStructure(mt.formatPtr, vih2);
                bmi = vih2.BmiHeader;
            }

            if (mt.formatType == FormatType.WaveEx)
            {
                wf = new WaveFormatEx();
                Marshal.PtrToStructure(mt.formatPtr, wf);
            }

            if (mt.formatType == FormatType.MpegVideo)
            {
                // 構造体の型が不明
            }

            if (mt.formatType == FormatType.Mpeg2Video)
            {
                // 構造体の型が不明
            }

            StringBuilder formatInfoSb = new StringBuilder();
            if (bmi != null)
            {
                GetBitmapInfoStr(bmi, formatInfoSb);
            }

            if (vih != null)
            {
                GetVideoInfoStr(vih, formatInfoSb);
            }

            if (vih2 != null)
            {
                GetVideoInfo2Str(vih2, formatInfoSb);
            }

            if (wf != null)
            {
                GetWaveFormatStr(wf, formatInfoSb);
            }

            Console.WriteLine("{0} {1} {2} {3}", majorTypeStr, subTypeStr, formatTypeStr, formatInfoSb);
        }
    }
}
