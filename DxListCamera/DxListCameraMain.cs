﻿using System;

namespace DxListCamera
{
    class DxListCameraMain
    {
        private const string TITLE = "List up CAMERA program by DirectShow Ver.0.10";

        private const string USAGE = "Usage : list_camera [-1] [<camera no.>]\n"
            + "Option : -1 : display mediatypes for 'One' pin (old version mode)\n";

        private static bool helpFlg = false;

        private static bool oneFlg = false;

        private static string name = null;

        private static int no = -1;

        static void Main(string[] args)
        {
            Console.WriteLine(TITLE);

            getOption(args);

            if (helpFlg)
            {
                Console.WriteLine(USAGE);
                Environment.Exit(1);
                return;
            }

            if (name != null)
            {
                if (!Int32.TryParse(name, out no))
                {
                    no = CameraList.SearchCamera(name);
                }

                Console.WriteLine("Search camera name '{0}' -> {1}", name, no);
            }

            CameraList.ListCamera(no, oneFlg);
        }

        private static void getOption(string[] args)
        {
            foreach (string arg in args)
            {
                if (arg == "-h")
                {
                    helpFlg = true;
                }
                if (arg == "-1")
                {
                    oneFlg = true;
                }
                else
                {
                    name = arg;
                    break;
                }
            }
        }
    }
}
