﻿using System;
using System.Drawing;
using System.Windows.Forms;
using DirectShowLib;

namespace DxCameraCapture
{
    public partial class Form1 : Form
    {
        const int VIDEODEVICE = 0; // zero based index of video capture device to use
        const int FRAMERATE = 30;  // Depends on video device caps.  Generally 4-30.
//        const int FRAMERATE = 15;  // Depends on video device caps.  Generally 4-30.
        const int VIDEOWIDTH = 1280; // Depends on video device caps
        const int VIDEOHEIGHT = 720; // Depends on video device caps
//        const int VIDEOHEIGHT = 960; // Depends on video device caps
        Guid FORMAT = MediaSubType.RGB24;
        //Guid FORMAT = MediaSubType.MJPG;
        //Guid FORMAT = MediaSubType.YUY2;

        Capture cam = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (cam == null)
            {
                cam = new Capture(VIDEODEVICE, FRAMERATE, VIDEOWIDTH, VIDEOHEIGHT, FORMAT);
                cam.SetCaptureCallBack(new Capture.CaptureCallBack(Capture_CallBack));
                cam.Start();
                btnStartStop.Text = "Stop";
            }
            else
            {
                cam.Dispose();
                cam = null;
                btnStartStop.Text = "Start";
            }
            Cursor.Current = Cursors.Default;
        }

        public void Capture_CallBack(Image img)
        {
            pctBuffer.Image = img;
        }
    }
}
